<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'max:200',
                'min:1'
            ],
            'start_date' => [
                'required',
                'date_format:format,Y-m-d H:i:s',
            ],
            'end_date' => [
                'required',
                'date_format:format,Y-m-d H:i:s',
            ]
        ];
    }
}
