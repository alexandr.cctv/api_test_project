<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{

    public const HOUR_IN_SECONDS = 3600;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => [
                'max:200',
                'min:1'
            ],
            'start_date' => [
                'date_format:format,Y-m-d H:i:s',
            ],
            'end_date' => [
                'date_format:format,Y-m-d H:i:s',
                'after:start_date'
            ]
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator): void {
            if($validator->errors()->isEmpty() &&
                Carbon::parse($this->end_date)->diffInSeconds(Carbon::parse($this->start_date)) >= self::HOUR_IN_SECONDS*12
            ) {
                $validator->errors()->add('end_date', 'End date should be les then start date plus 12 hours');
            }
        });
    }
}
