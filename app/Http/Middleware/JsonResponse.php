<?php
# app/Http/Middleware/JsonResponse.php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class JsonResponse {

    /**
     * Set 'Accept' header to force a JSON response for API routes.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->add([
            'accept' => 'application/json'
        ]);
        return $next($request);
    }

}
