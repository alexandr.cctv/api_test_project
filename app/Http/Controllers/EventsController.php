<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventStoreRequest;
use App\Http\Requests\EventUpdateRequest;
use App\Http\Resources\Collections\EventCollection;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Repositories\EventRepository;
use Illuminate\Support\Facades\Auth;

class EventsController extends Controller
{
    public function list(EventRepository $eventRepository): EventCollection
    {
        return new EventCollection($eventRepository->getAvailableEvents());
    }

    public function show(Event $event): EventResource
    {
        $this->authorize('show', $event);

        return new EventResource($event);
    }

    public function store(EventStoreRequest $eventStoreRequest): EventResource
    {
        $this->authorize('store', Event::class);
        $newEvent = new Event();
        $newEvent->fill($eventStoreRequest->validated());
        $newEvent->organization_id = Auth::user()->tokens->first()->id;
        $newEvent->save();
        return new EventResource($newEvent);
    }

    public function update(Event $event, EventUpdateRequest $eventUpdateRequest): EventResource
    {
        $this->authorize('update', $event);
        $event->fill($eventUpdateRequest->validated());
        $event->save();
        return new EventResource($event);
    }

    public function delete(Event $event): EventResource
    {
        $this->authorize('delete', $event);

        return new EventResource($event);
    }
}
