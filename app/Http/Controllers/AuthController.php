<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthController extends Controller
{
    public function login(LoginRequest $loginRequest)
    {
        if (Auth::attempt($loginRequest->validated())) {
            $loginRequest->session()->regenerate();
            $token = Auth::user()->createToken($loginRequest->device_name ?? '');

            return Response::json(['token' => $token->plainTextToken]);
        }
        return Response::json([
            'message' => 'The provided credentials do not match our records.',
        ], JsonResponse::HTTP_UNAUTHORIZED);
    }
}
