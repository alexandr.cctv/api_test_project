<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;

class EventPolicy
{
    public function show(User $user, Event $event): bool
    {
        return $this->checkAccess($user, $event);
    }

    public function store(User $user): bool
    {
        return true;
    }

    public function update(User $user, Event $event): bool
    {
        return $this->checkAccess($user, $event);
    }

    public function delete(User $user, Event $event): bool
    {
        return $this->checkAccess($user, $event);
    }

    private function checkAccess(User $user, Event $event): bool
    {
        return $user->tokens()->get()->pluck('id')->contains($event->organization_id);
    }
}
