<?php

namespace App\Repositories;

use App\Models\Event;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class EventRepository
{
    public function getAvailableEvents(): Collection
    {
        return Event::whereIn('organization_id', Auth::user()->tokens->pluck('id'))->get();
    }
}
