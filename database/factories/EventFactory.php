<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $startDate = fake()->dateTime;
        $endDate = (clone $startDate)->add(new \DateInterval('PT1H'));

        return [
            'title' => fake()->text(150),
            'start_date' => $startDate,
            'end_date' => $endDate
        ];
    }
}
