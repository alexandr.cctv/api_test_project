git clone repository

cd project directory

cp .env.example .env

docker compose up

docker exec -ti app_api_test_project bash

php artisan test
