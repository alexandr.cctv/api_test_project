<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_csrf(): void
    {
        $response = $this->get('/sanctum/csrf-cookie');

        $response->assertStatus(204);
    }

    public function test_login(): void
    {
        $response = $this->get('/sanctum/csrf-cookie');

        $response->assertStatus(204);

        $user = User::factory()->createOne(['password' => 'testtest']);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'testtest'
        ]);

        $response->assertStatus(200);
        $response->assertJsonMissingExact(['token']);
    }
}
