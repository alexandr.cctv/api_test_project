<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class EventsTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    public function test_list(): void
    {
        $this->setUser();

        Sanctum::actingAs(
            $this->user,
            ['*']
        );

        $events = Event::factory(2)->create(['organization_id' => $this->user->tokens()->first()->id]);

        $response = $this->get('/api/event/list');

        $response->assertStatus(200);

        $response
            ->assertJson(function (AssertableJson $json) use ($events) {
                $event = $events->first();
                $json->where('data.0.id', $event->id)
                    ->where('data.0.title', $event->title);
            });
    }

    public function test_show(): void
    {
        $this->setUser();

        Sanctum::actingAs(
            $this->user,
            ['*']
        );

        $event = Event::factory()->create(['organization_id' => $this->user->tokens()->first()->id]);

        $response = $this->get('/api/event/'.$event->id);

        $response->assertStatus(200);

        $response
            ->assertJson(function (AssertableJson $json) use ($event) {
                $json->where('data.id', $event->id)
                    ->where('data.title', $event->title);
            });
    }

    public function test_store(): void
    {
        $this->setUser();

        Sanctum::actingAs(
            $this->user,
            ['*']
        );

        $event = Event::factory()->make();

        $response = $this->post('/api/event/', [
            'title' => $event->title,
            'start_date' => $event->start_date->format('Y-m-d H:i:s'),
            'end_date' => $event->end_date->format('Y-m-d H:i:s')
        ]);

        $response->assertStatus(201);

        $response
            ->assertJson(function (AssertableJson $json) use ($event) {
                $json->has('data.id')
                    ->where('data.title', $event->title)
                    ->where('data.start_date', $event->start_date->format('Y-m-d H:i:s'))
                    ->where('data.end_date', $event->end_date->format('Y-m-d H:i:s'));
            });
    }

    public function test_update(): void
    {
        $this->setUser();

        Sanctum::actingAs(
            $this->user,
            ['*']
        );

        $event = Event::factory()->create(['organization_id' => $this->user->tokens()->first()->id]);
        $event->title .= '_updated';

        $response = $this->patch('/api/event/'.$event->id, [
            'title' => $event->title,
            'start_date' => $event->start_date->format('Y-m-d H:i:s'),
            'end_date' => $event->end_date->format('Y-m-d H:i:s')
        ]);

        $response->assertStatus(200);

        $response
            ->assertJson(function (AssertableJson $json) use ($event) {
                $json->has('data.id')
                    ->where('data.title', $event->title)
                    ->where('data.start_date', $event->start_date->format('Y-m-d H:i:s'))
                    ->where('data.end_date', $event->end_date->format('Y-m-d H:i:s'));
            });

        $response = $this->patch('/api/event/'.$event->id, [
            'title' => Str::random(201),
        ]);

        $response->assertStatus(422);

        $response
            ->assertJson(function (AssertableJson $json) {
                $json->has('message')
                    ->has('errors.title');
            });

        $response = $this->patch('/api/event/'.$event->id, [
            'end_date' => (clone $event->start_date)->add(new \DateInterval('PT13H'))->format('Y-m-d H:i:s')
        ]);

        $response->assertStatus(422);

        $response
            ->assertJson(function (AssertableJson $json) {
                $json->has('message')
                    ->has('errors.end_date');
            });
    }

    public function test_delete(): void
    {
        $this->setUser();

        Sanctum::actingAs(
            $this->user,
            ['*']
        );

        $event = Event::factory()->create(['organization_id' => $this->user->tokens()->first()->id]);

        $response = $this->delete('/api/event/'.$event->id);

        $response->assertStatus(200);

        $response
            ->assertJson(function (AssertableJson $json) use ($event) {
                $json->where('data.id', $event->id);
            });
    }

    private function setUser(): void
    {
        $response = $this->get('/sanctum/csrf-cookie');

        $response->assertStatus(204);

        $this->user = User::factory()->createOne(['password' => 'testtest']);

        $this->authorizeUser();
    }

    private function authorizeUser(): void
    {
        $response = $this->post('/login', [
            'email' => $this->user->email,
            'password' => 'testtest'
        ]);

        $response->assertStatus(200);
        $response->assertJsonMissingExact(['token']);
    }
}
