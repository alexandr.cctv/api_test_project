<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MainTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_main_page(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_get_user(): void
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        $response = $this->get('/api/user');

        $response->assertStatus(200);
        $response->assertJson(fn(AssertableJson $json) => $json->hasAll([
            'id',
            'name',
            'email',
            'email_verified_at',
            'updated_at',
            'created_at'
        ])
        );
    }
}
