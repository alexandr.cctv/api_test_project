<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::controller(EventsController::class)->prefix('event')->group(function(){
        Route::get('list', 'list');
        Route::get('{event}', 'show');
        Route::post('/', 'store');
        Route::patch('{event}', 'update');
        Route::delete('{event}', 'delete');
    });
});
